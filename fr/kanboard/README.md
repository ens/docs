Documentation
=============

Utiliser Kanboard
-----------------

### Introduction

- [Qu'est-ce que Kanban ?](what-is-kanban.html)
- [Comparons Kanban aux Todo listes et à Scrum](kanban-vs-todo-and-scrum.html)
- [Exemples d'utilisation](usage-examples.html)

### Utiliser un tableau

- [Vues Tableau, Agenda et Liste](project-views.html)
- [Mode Replié et Déplié](board-collapsed-expanded.html)
- [Défilement horizontal et mode compact](board-horizontal-scrolling-and-compact-view.html)
- [Afficher ou cacher des colonnes dans le tableau](board-show-hide-columns.html)

### Travailler avec les projets

- [Types de projets](project-types.html)
- [Créer des projets](creating-projects.html)
- [Modifier des projets](editing-projects.html)
- [Supprimer des projets](removing-projects.html)
- [Partager des tableaux et des tâches](sharing-projects.html)
- [Actions automatiques](automatic-actions.html)
- [Permissions des projets](project-permissions.html)
- [Swimlanes](swimlanes.html)
- [Calendriers](calendar.html)
- [Analytique](analytics.html)
- [Diagramme de Gantt pour les tâches](gantt-chart-tasks.html)
- [Diagramme de Gantt pour tous les projets](gantt-chart-projects.html)
- [Rôles personnalisés pour les projets](custom-project-roles.html)

### Travailler avec les tâches

- [Créer des tâches](creating-tasks.html)
- [Fermer des tâches](closing-tasks.html)
- [Dupliquer et déplacer des tâches](duplicate-move-tasks.html)
- [Ajouter des captures d'écran](screenshots.html)
- [Liens internes entre les tâches](task-links.html)
- [Transitions](transitions.html)
- [Suivi du temps](time-tracking.html)
- [Tâches récurrentes](recurring-tasks.html)
- [Créer des tâches par email](create-tasks-by-email.html)
- [Sous-tâches](subtasks.html)
- [Analytique des tâches](analytics-tasks.html)
- [Mentionner les utilisateurs](user-mentions.html)

### Travailler avec les utilisateurs

- [Rôles](roles.html)
- [Gestion des utilisateurs](user-management.html)
- [Notifications](notifications.html)
- [Authentification à deux facteurs](2fa.html)

### Paramètres

- [Raccourcis clavier](keyboard-shortcuts.html)
- [Paramètres de l'application](application-configuration.html)
- [Paramètres du projet](project-configuration.html)
- [Paramètres du tableau](board-configuration.html)
- [Paramètres du calendrier](calendar-configuration.html)
- [Paramètres du lien](link-labels.html)
- [Taux de change](currency-rate.html)

### Pour aller plus loin&nbsp;:
  * [Déframasoftiser Internet](deframasoftiser.html)
  * [Essayer Framaboard](https://framaboard.org)
  * Application Android&nbsp;:
    * [Kanboard](https://f-droid.org/packages/in.andres.kandroid/) sur F-droid
  * [Dégooglisons Internet](https://degooglisons-internet.org)
  * [Soutenir Framasoft](https://soutenir.framasoft.org)
