**Attention : Travail en cours**

Voici un manuel en cours d'élaboration collaborative. La peinture est fraîche, il va sûrement y avoir des fôtes et coquilles, et le tout est voué à évoluer.

****

# Comprendre le numérique (avec les doigts)

## « Donnez-moi un exemple simple à comprendre »

Les membres de Framasoft, et plus généralement les libristes, vont régulièrement au devant des gens pour expliquer comment fonctionne le monde numérique.

Nous essayons de rassembler ici les exemples et métaphores que nous utilisons lors de ces rencontres, et qui semblent être à la fois justes et parlants.


## Sommaire
_il faudra rassembler les exemples par thématiques au fur et a mesure_


* [Comprendre la connexion sécurisée, le VPN et Tor (avec des suppositoires)](https-vpn.md)
* [Comprendre les pages et navigateurs web (avec TopChef)](navigateur-web.md)
* [Comprendre l’hébergement d’application par les plantes](comprendre-hebergement-applications-par-les-plantes.md)
* [Le Web, c’est comme une livraison de Pizza (ou presque)](web-livraison-pizza.md)
* [Vulgarisation sur https et TOR](vulgarisation-https-tor.md)
* [Comprendre le téléchargement en torrent](torrent.md)
* [Comprendre ActivityPub et la fédération (avec des villages de moustachus)](https://framablog.org/2018/05/23/comment-reparer-les-medias-sociaux-et-faire-encore-mieux/)
* [Comprendre les APIs (avec des marchands de tomate)](https://framablog.org/2018/05/23/comment-reparer-les-medias-sociaux-et-faire-encore-mieux/#comment-75908)
