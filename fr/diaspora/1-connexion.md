Partie 1 – Connexion
--------------------

Bienvenue sur notre guide de "Démarrage". Si vous êtes arrivés ici,
c'est que vous venez juste de vous inscrire sur diaspora\* ou que vous
pensez à le faire. Dans cette série de tutoriels nous allons vous guider
dans le processus d'inscription et vous aider à vous familiariser avec
les bases de fonctionnement de diaspora\*. Nous espérons que vous le
trouverez utile et que vous prendrez plaisir à utiliser diaspora\* !

## Trouver un pod

Si vous êtes déjà connecté(e), vous pouvez passer cette partie et aller directement à la [Partie 2](2-interface.html).

Contrairement à la plupart des sites de réseaux sociaux gérés par une
concentration d'ordinateurs qui stockent les données de tout le monde,
nous ne pouvons pas vous donner un simple lien et vous dire : «&nbsp;inscrivez-vous ici&nbsp;».
diaspora\* est différent de ces réseaux car il est
**décentralisé** : vous pouvez vous y connecter depuis un grand nombre de
points d'accès et, avant de commencer, vous allez devoir choisir auprès
duquel vous inscrire. On appelle ces points d'accès des "pods".

Le mot "diaspora" fait référence à la dispersion de graines (ou de
personnes) à travers une large zone. C'est pourquoi notre motif est le
pissenlit et l'astérisque représente une aigrette de pissenlit. On
appelle les comptes individuels sur diaspora\* des "**graines**" et les
serveurs qui hébergent ces comptes des "**pods**". Vous vous y
habituerez rapidement !

Pour vous aider à trouver le pod qui vous conviendra le mieux, jetez un
œil à notre [Guide pour choisir un
pod](https://wiki.diasporafoundation.org/Choosing_a_pod).

Vous avez peut être reçu une invitation à diaspora\* d'une personne que
vous connaissez. Si c'est le cas, elle contient un lien vers le pod
diaspora\* sur lequel la personne qui vous a invitée est enregistrée. Si
vous voulez rejoindre le même pod qu'elle, suivez simplement le lien
fourni dans l'e-mail. Toutefois, vous n'êtes pas obligés d'être sur le
même pod que vos amis pour communiquer avec eux.

Vous vous connecterez toujours à diaspora\* via le pod auquel vous vous
serez inscrit(e). Depuis ce pod, vous aurez accès à l'ensemble du réseau
et votre expérience de diaspora\* sera plus ou moins la même, quel que
soit le pod depuis lequel vous vous connecterez. Vous ne pourrez pas
vous connecter sur un autre pod mais ça ne servirait à rien de toute
façon.

## Inscription

Une fois que vous aurez choisi votre pod, cherchez le lien inscription.
Il devrait se trouver juste là, sur la page d'accueil du pod. Si vous ne
voyez pas de lien pour s'inscrire, il est possible que ce pod n'accepte
pas de nouvelle inscription pour le moment. Dans ce cas, choisissez
simplement un autre pod.

Quand vous cliquerez sur le lien d'inscription, vous arriverez sur une
page qui vous demandera un nom d'utilisateur, une adresse email et un
mot de passe. Choisissez soigneusement votre nom d'utilisateur car vous
ne pourrez pas le changer ultérieurement.

![Signup](images/connexion-1.png)

Terminez l'inscription en cliquant sur Continuer pour créer votre toute
nouvelle graine de diaspora\* et y accéder.

Pendant la phase d'inscription, votre graine sera probablement
automatiquement connectée à une autre graine. Ce sera la
graine du [*podmin* Framasphère](https://framasphere.org/u/podmin) (c'est-à-dire de l'administrateur de votre pod). Cela nous
permettra de vous tenir informé(e) des annonces importantes concernant
Framasphère. Si toutefois vous préférez ne pas suivre cette graine, vous
pouvez la retirer facilement de votre liste de contacts. Vous trouverez
comment faire ici [Partie 4](4-echange.html).

Vous devriez maintenant aboutir à une page "Pour commencer". Ce n'est
pas indispensable, mais la remplir vous aidera énormément à commencer à
établir des connexions avec d'autres personnes.

![Gettingstarted](images/connexion-2.png)

1.  Entrez le nom que vous souhaitez voir apparaître à l'écran dans le
    premier champ.
2.  Cliquez sur le bouton pour télécharger une photo de profil à partir
    de votre ordinateur.
3.  Ajoutez quelques mots qui représentent des choses qui vous
    intéressent. Cela peut être la musique, l'activisme, peut-être la
    ville où vous habitez. Ces mots seront convertis en \#tags qui vous
    aideront à trouver du contenu et à d'autres personnes de vous
    découvrir. Nous expliquerons les \#tags plus en détail dans la suite
    de ce tutoriel.

Avant de faire quoique ce soit, prenez une minute pour compléter votre
profil. Sur la page où vous avez été amené(e) cliquez sur votre nom ou
votre photo sur le côté droit de la barre d'en-tête noire et
sélectionnez Profil dans la liste qui apparaît. Cliquez ensuite sur le
bouton bleu Modifier mon profil en haut à droite.

Il y a deux parties dans votre profil : le profil public et le profil
privé. Votre profil public est visible de tout le monde ; votre profil
privé n'est visible que par les personnes avec qui vous avez choisi de
partager. Vous pouvez remplir autant ou aussi peu votre profil que vous
le voulez, et vous n'êtes pas obligé(e) d'utiliser votre vrai nom si
vous ne le souhaitez pas.

![Profile
basic](images/connexion-3.png)

![Profile
extended](images/connexion-4.png)

La plupart des champs d'information du profil sont explicites. NSFW
signifie "not safe for work". Nous parlerons de cela plus en détail dans
[Partie 7](7-fin.html), ne
vous en ennuyez pas trop avec cela pour l'instant.

N'oubliez pas de cliquer sur le bouton **Mettre à jour le profil** une fois
que vous avez terminé !

## C'est tout !

Vous êtes prêt(e) à utiliser diaspora\* ! Cliquez sur le logo en
astérisque ou sur Flux en haut à gauche de votre écran pour commencer.

Si vous souhaitez rencontrer et être accueilli(e) par la communauté
diaspora\* et si vous vous sentez suffisamment à l'aise, écrivez un
message **public** pour vous présenter et incluez le tag \#nouveauici ou \#nouvelleici.
Écrire des messages publics contenant des tags est une excellente
manière de vous faire de nouveaux amis sur diaspora\*. Si vous ne vous
sentez pas encore assez sûr(e), ne vous inquiétez pas ; nous vous
aiderons pas-à-pas à publier votre premier message ici [Partie
5](5-partage.html).

[2ème partie -
L'interface](2-interface.html)

### Ressources utiles

-   [Base de code](http://github.com/diaspora/diaspora/)
-   [Documentation](https://wiki.diasporafoundation.org/)
-   [Trouver et signaler des
    bugs](http://github.com/diaspora/diaspora/issues)
-   [IRC - Général](http://webchat.freenode.net/?channels=diaspora)
-   [IRC -
    Développement](http://webchat.freenode.net/?channels=diaspora-dev)
-   [Liste - Général](http://groups.google.com/group/diaspora-discuss)
-   [Liste - Développement](http://groups.google.com/group/diaspora-dev)


[![Licence Creative
Commons](images/cc_by.png)](http://creativecommons.org/licenses/by/3.0/)
[diasporafoundation.org](https://diasporafoundation.org/) est protégé
sous licence [Licence publique Creative Commons Attribution 3.0 non
transposée](http://creativecommons.org/licenses/by/3.0/)
